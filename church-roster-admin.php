<?php

/* Admin section */
add_action('admin_menu', 'church_roster_menu');

function church_roster_menu() {
	add_options_page('Church Roster Options', 'Church Roster', 'manage_options', 'church_roster', 'church_roster_options');
}

function church_roster_options() {
	if (!current_user_can('manage_options')) {
		wp_die(__('You do not have sufficient permissions to access this page.'));
	}

	?>
<div class="wrap">
<?php screen_icon(); ?>
<h2><?php _e('Church Roster'); ?></h2>

<form method="post" action="options.php"> 
<table class="form-table">
<tbody>
<tr valign="top">
<th scope="row"><label for="church_roster_sms_url"><?php _e('SMS URL'); ?></label></th>
<td><input type="text" class="regular-text" name="church_roster_sms_url" id="church_roster_sms_url" value="<?php echo get_option('church_roster_sms_url'); ?>"0>
<p class="description">e.g. http://inteltech.com.au/secure-api/send.php?method=http&amp;username=exampleuser&amp;key=69e079bbe765fer744e5bd296adfg648d0b74230a31&amp;sms=%phone%&amp;message=%message%&amp;senderid=MyChurch</p>
</td>
</tr>
<tr valign="top">
<th scope="row"><label for="church_roster_sms_day"><?php _e('Days Before Event Send SMS'); ?></label></th>
<td><input type="number" step="1" min="-1" name="church_roster_sms_day" id="church_roster_sms_day" value="<?php echo get_option('church_roster_sms_day'); ?>">
<p class="description">Number of days before an event to send an SMS (-1 to disable)</p>
</td>
</tr>
<tr valign="top">
<th scope="row"><label for="church_roster_email_day"><?php _e('Days Before Event Send Email'); ?></label></th>
<td><input type="number" step="1" min="-1" name="church_roster_email_day" id="church_roster_email_day" value="<?php echo get_option('church_roster_email_day'); ?>">
<p class="description">Number of days before an event to send an Email (-1 to disable)</p>
</td>
</tr>
</table>

<?php submit_button(); ?>
</form>
</div>

<?php
}


function church_roster_submenu() {
	$plugin_page = add_submenu_page('edit.php?post_type='.EM_POST_TYPE_EVENT, 'Event Roles', 'Church Roster Roles', 'edit_events', "events-manager-church-roster-roles", 'church_roster_roles_page');
}
add_action('admin_menu','church_roster_submenu', 20);


function church_roster_roles_page() {
	global $wpdb, $EM_Event, $EM_Notices;

	$table_name = $wpdb->prefix . 'church_roster_roles';

	if (!empty($_REQUEST['action'])) {
		if ($_REQUEST['action'] == "church_roster_roles_save" && wp_verify_nonce($_REQUEST['_wpnonce'], 'church_roster_roles_save')) {
			// Update the table
			if (!empty($_REQUEST['role_id'])) {
				$wpdb->update($table_name, array(
						'role_id' => $_REQUEST['role_id'],
						'role_name' => $_REQUEST['role_name'],
						'role_weight' => $_REQUEST['role_weight']
					), 
					array('role_id' => $_REQUEST['role_id']),
					array('%d', '%s', '%d'),
					array('%d')
				);
			} 
			else {
				$wpdb->insert($table_name, array(
						'role_name' => $_REQUEST['role_name'],
						'role_weight' => $_REQUEST['role_weight']
					),
					array('%s', '%d')
				);
			}
		}
		elseif ($_REQUEST['action'] == "church_roster_roles_delete" && wp_verify_nonce($_REQUEST['_wpnonce'], 'church_roster_roles_delete')) {
			// Delete from the table
			if ($_REQUEST['role_id']) {
				$wpdb->delete($table_name, array('role_id' => $_REQUEST['role_id']), array('%d'));
			}
		}
	}
	church_roster_roles_table_layout();
}

function church_roster_roles_table_layout() {
	global $wpdb, $EM_Notices;
	
	$church_roster_roles = $wpdb->get_results('SELECT role_id, role_name, role_weight FROM ' . $wpdb->prefix . 'church_roster_roles ORDER BY role_weight');
	
	$edit_role = $wpdb->get_row($wpdb->prepare('SELECT role_id, role_name, role_weight FROM ' . $wpdb->prefix . 'church_roster_roles WHERE role_id=%d', $_REQUEST['role_id']));

	?>
	<div class='wrap'>
		<div id='icon-edit' class='icon32'>
			<br/>
		</div>
		<h2><?php _e('Church Roster', 'church_roster')?></h2>
		<?php echo $EM_Notices; ?>
		<div id='col-container'>
			<!-- begin col-right -->
			<div id='col-right'>
			 	<div class='col-wrap'>
				 	 <form id='bookings-filter' method='post' action=''>
						<input type='hidden' name='action' value='church_roster_role_delete'/>
						<input type='hidden' name='_wpnonce' value='<?php echo wp_create_nonce('church_roster_roles_delete'); ?>' />
						<?php if (count($church_roster_roles)>0) : ?>
							<table class='widefat'>
								<thead>
									<tr>
										<th class='manage-column column-cb check-column' scope='col'><input type='checkbox' class='select-all' value='1'/></th>
										<th><?php _e('ID', 'church_roster') ?></th>
										<th><?php _e('Name', 'church_roster') ?></th>
										<th><?php _e('Weight', 'church_roster')?>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th class='manage-column column-cb check-column' scope='col'><input type='checkbox' class='select-all' value='1'/></th>
										<th><?php _e('ID', 'church_roster') ?></th>
										<th><?php _e('Name', 'church_roster') ?></th>
										<th><?php _e('Weight', 'church_roster')?>
									</tr>
								</tfoot>
								<tbody>
									<?php foreach ($church_roster_roles as $church_roster_role) : ?>
									<tr>
										<td><input type='checkbox' class ='row-selector' value='<?php echo $church_roster_role->role_id ?>' name='role_id[]'/></td>
										<td><a href='<?php echo get_bloginfo('wpurl') ?>/wp-admin/admin.php?page=events-manager-church-roster-roles&amp;action=edit&amp;role_id=<?php echo $church_roster_role->role_id ?>'><?php echo $church_roster_role->role_id; ?></a></td>
										<td><a href='<?php echo get_bloginfo('wpurl') ?>/wp-admin/admin.php?page=events-manager-church-roster-roles&amp;action=edit&amp;role_id=<?php echo $church_roster_role->role_id ?>'><?php echo htmlspecialchars($church_roster_role->role_name, ENT_QUOTES); ?></a></td>
										<td><a href='<?php echo get_bloginfo('wpurl') ?>/wp-admin/admin.php?page=events-manager-church-roster-roles&amp;action=edit&amp;role_id=<?php echo $church_roster_role->role_id ?>'><?php echo $church_roster_role->role_weight; ?></a></td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>

							<div class='tablenav'>
								<div class='alignleft actions'>
							 	<input class='button-secondary action' type='submit' name='doaction2' value='<?php _e('Delete', 'church_roster') ?>'/>
								<br class='clear'/>
								</div>
								<br class='clear'/>
							</div>
						<?php else: ?>
							<p><?php _e('No roster roles defined yet!', 'church_roster') ?></p>
						<?php endif; ?>
					</form>
				</div>
			</div>
			<!-- end col-right -->

			<!-- begin col-left -->
			<div id='col-left'>
				<div class='col-wrap'>
					<div class='form-wrap'>
						<div id='ajax-response'>
							<h3><?php echo empty($_REQUEST['role_id']) ? __('Add Role', 'church_roster'):__('Update Role', 'church_roster'); ?></h3>
							<form name='add' id='add' method='post' action='' class='add:the-list: validate'>
								<input type='hidden' name='action' value='church_roster_roles_save' />
								<input type='hidden' name='_wpnonce' value='<?php echo wp_create_nonce('church_roster_roles_save'); ?>' />
								<div class='form-field form-required'>
									<label for='role-name'><?php _e('Role Name', 'church_roster') ?></label>
									<?php if (!empty($edit_role)): ?>
									<input id='role-name' name='role_name' type='text' size='40' value="<?php echo $edit_role->role_name; ?>" />
									<input id='role-id' name='role_id' type='hidden' value="<?php echo $_REQUEST['role_id']; ?>" />
									<?php else: ?>
									<input id='role-name' name='role_name' type='text' size='40' />
									<?php endif; ?>
								</div>
								<div class='form-field form-required'>
									<label for='role-weight'><?php _e('Role Weight', 'church_roster') ?></label>
									<?php if ($edit_role): ?>
									<input id='role-weight' name='role_weight' type='text' size='40' value="<?php echo $edit_role->role_weight; ?>" />
									<?php else: ?>
									<input id='role-weight' name='role_weight' type='text' size='40' />
									<?php endif; ?>
								</div>
								<p class='submit'>
									<?php if( !empty($_REQUEST['role_id']) ): ?>
									<input type='submit' class='button' name='submit' value='Update Role' />
									or <a href="admin.php?page=events-manager-church-roster-roles">Add New</a>
									<?php else: ?>
									<input type='submit' class='button' name='submit' value='Add Role' />
									<?php endif; ?>
								</p>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- end col-left -->
		</div>
	</div>
	<?php
}

?>