=== church_roster ===
Contributors: mosestruong
Donate link: http://www.truong.id.au/
Tags: church roster, sms, email
Requires at least: 3.0.0
Tested up to: 3.5.2
Stable tag: 0.1.0

A plugin for church rosters, including reminders sent via email and sms. 
This plugin requires Event Manager plugin written by Marcus Sykes (http://wp-events-plugin.com/)

== Description ==

This plugin is for church wordpress site allows rosters to be defined for events created
using the Event Manager (tested with Events Manager v5.4.4).

= Main Features =

* Flexible - define your own rostered positions
* Bulk import - create rosters from CSV/XLS/ODS spreadsheet
* Email - send a reminder email to people on the roster
* SMS - send SMS to people on the roster (Currently only works with Inteltech - http://inteltech.com.au/)

== Installation ==

1. Upload the `church_roster` directory to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Place [church_roster type=event event_id=#] on the page you want the roster displayed for event 

== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==
= 0.1 =
* Initial release

== Upgrade Notice ==

== Credits: ==
