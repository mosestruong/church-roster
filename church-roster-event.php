<?php

function church_roster_meta_boxes() {
	add_meta_box('church_roster', 'Roster', 'church_roster_metabox', EM_POST_TYPE_EVENT, 'side', 'low');
}
add_action('add_meta_boxes', 'church_roster_meta_boxes');

function church_roster_metabox(){
	global $wpdb, $EM_Event;

	$church_roster_roles = $wpdb->get_results('SELECT role_id, role_name, role_weight FROM ' . $wpdb->prefix . 'church_roster_roles ORDER BY role_weight');
	
	?>
	<select name="event_roster_role">
		<option value="0">Select Role</option>
	<?php 
	foreach ($church_roster_roles as $church_roster_role) {
		?>
		<option value="<?php echo $church_roster_role->role_id; ?>"><?php echo $church_roster_role->role_name; ?></option>
		<?php
	}
	?>
	</select>
	<?php 
	
	$users = get_users();
	?>
		<select name="event_roster_user">
			<option value="0">Select Person</option>
		<?php 
		foreach ($users as $user) {
			?>
			<option value="<?php echo $user->user_id; ?>"><?php echo $user->user_nicename; ?></option>
			<?php
		}
		?>
		</select>
		<?php 
		
}
?>