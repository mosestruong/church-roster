<?php

/* Activation/Deactivation Hooks */
register_activation_hook(__FILE__, 'church_roster_install');
register_deactivation_hook(__FILE__, 'church_roster_uninstall');

function church_roster_install() {
	/* Create new database fields */
	update_option('church_roster_sms_url', 'http://inteltech.com.au/secure-api/send.php?method=http&username=exampleuser&key=examplekey&sms=%phone%&message=%message%&senderid=%sender%');
	update_option('church_roster_sms_day', 3);
	update_option('church_roster_email_day', 10);

	// Create tables to store the roster
	church_roster_create_table();
}

function church_roster_uninstall() {
	/* Remove the database field */
	delete_option('church_roster_sms_url');
	delete_option('church_roster_sms_day');
	delete_option('church_roster_email_day');
}

function church_roster_create_table() {
	global $wpdb;
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

	$table_name = $wpdb->prefix . 'church_roster_roles';
	$sql = "CREATE TABLE " . $table_name . " (
		role_id smallint(6) NOT NULL AUTO_INCREMENT,
		role_name varchar(50) NOT NULL,
		role_weight tinyint(4) NOT NULL,
		PRIMARY KEY (role_id)
		) DEFAULT CHARSET=utf8";
	dbDelta($sql);

	$table_name = $wpdb->prefix . 'church_roster_users';
	$sql = "CREATE TABLE " . $table_name . " (
		event_id bigint(20) NOT NULL,
		role_id smallint(6) NOT NULL,
		user_id bigint(20) NOT NULL,
		KEY event_id (event_id),
		KEY user_id (user_id)
		) DEFAULT CHARSET=utf8";
	dbDelta($sql);
}

?>